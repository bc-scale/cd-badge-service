variable "region" {
  type        = string
  description = "The AWS region"
  default     = "eu-west-1"
}

variable "access_key" {
  type        = string
  description = "The AWS access key"
  default     = ""
  sensitive   = true
}

variable "secret_key" {
  type        = string
  description = "The AWS secret key"
  default     = ""
  sensitive   = true
}
