resource "aws_api_gateway_rest_api" "badges" {
  body = file("../openapi.yaml")

  name = "Badges"

  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_deployment" "badges" {
  rest_api_id = aws_api_gateway_rest_api.badges.id

  triggers = {
    redeployment = sha1(jsonencode(aws_api_gateway_rest_api.badges.body))
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "badges" {
  deployment_id = aws_api_gateway_deployment.badges.id
  rest_api_id   = aws_api_gateway_rest_api.badges.id
  stage_name    = "v1"
}

resource "aws_apigatewayv2_api_mapping" "badges" {
  api_id          = aws_api_gateway_rest_api.badges.id
  stage           = aws_api_gateway_stage.badges.stage_name
  domain_name     = "api.coderdojokinsale.com"
  api_mapping_key = "v1/badges"
}
